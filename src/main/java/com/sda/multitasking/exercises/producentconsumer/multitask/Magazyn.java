package com.sda.multitasking.exercises.producentconsumer.multitask;

import com.sda.multitasking.exercises.producentconsumer.Kolo;
import com.sda.multitasking.exercises.producentconsumer.RamaRowerowa;
import com.sda.multitasking.exercises.producentconsumer.Rower;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class Magazyn {
    private List<Kolo> kola = new ArrayList<>();
    private List<RamaRowerowa> ramy = new ArrayList<>();

    private List<Rower> rowery = new ArrayList<>();

    public void dodajKoloDoMagazynu(Kolo noweKolo) {
        kola.add(noweKolo);
        System.out.println("Stan magazynu, ilość kół:" + kola.size());
    }

    public void dodajRameDoMagazynu(RamaRowerowa nowaRama) {
        ramy.add(nowaRama);
        System.out.println("Stan magazynu, ilość ram rowerowych:" + ramy.size());
    }

    public boolean wystarczyKomponentówNaJedenRower() {
        return !ramy.isEmpty() && kola.size() >= 2;
    }

    public Kolo[] pobierzKola() {
        return new Kolo[]{kola.remove(0), kola.remove(0)};
    }

    public RamaRowerowa pobierzRame() {
        return ramy.remove(0); // usuwa i zwraca obiekt
    }

    public void dodajRowerDoMagazynu(Rower rower) {
        rowery.add(rower);
        System.out.println("Stan magazynu, ilość rowerów:" + rowery.size());
    }
}
