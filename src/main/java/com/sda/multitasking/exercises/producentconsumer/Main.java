package com.sda.multitasking.exercises.producentconsumer;

import com.sda.multitasking.exercises.producentconsumer.multitask.KoloProducer;
import com.sda.multitasking.exercises.producentconsumer.multitask.Magazyn;
import com.sda.multitasking.exercises.producentconsumer.multitask.RamaRowerowaProducer;
import com.sda.multitasking.exercises.producentconsumer.multitask.RowerProducer;

public class Main {
    public static void main(String[] args) {
        Magazyn magazyn = new Magazyn();

        new KoloProducer(magazyn, 5).start();

        new RamaRowerowaProducer(magazyn, 8).start();
        new RamaRowerowaProducer(magazyn, 8).start();

        new RowerProducer(magazyn, 1).start();
    }
}
