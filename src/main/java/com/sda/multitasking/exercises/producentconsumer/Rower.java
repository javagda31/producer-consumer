package com.sda.multitasking.exercises.producentconsumer;

import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
public class Rower {
    private LocalDateTime dataProdukcji;
    private Kolo[] kola = new Kolo[2]; // zawsze wielkości 2.
    private RamaRowerowa rama;
    private String producent;

    public Rower(RamaRowerowa rama, Kolo[] kola, String producent) {
        this.dataProdukcji = LocalDateTime.now(); // UWAGA ! data produkcji to data utworzenia.
        this.rama = rama;
        this.kola = kola;
        this.producent = producent;
    }
}
