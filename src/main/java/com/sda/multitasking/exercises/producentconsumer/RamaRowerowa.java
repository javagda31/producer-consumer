package com.sda.multitasking.exercises.producentconsumer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.sda.multitasking.exercises.producentconsumer.Material.ALUMINIUM;
import static com.sda.multitasking.exercises.producentconsumer.Material.WLOKNO_WEGLOWE;
import static com.sda.multitasking.exercises.producentconsumer.RodzajRamy.BMX;
import static com.sda.multitasking.exercises.producentconsumer.RodzajRamy.GÓRSKI;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RamaRowerowa {
    private double wysokość;
    private double długość;
    private RodzajRamy rodzajRamy;
    private Material material;

    public static RamaRowerowa stworzRameBMX(PrzeznaczenieWiekowe wiek) {
        switch (wiek) {
            case DZIECKO:
                return new RamaRowerowa(32.4, 83, BMX, ALUMINIUM);
            case NASTOLATEK:
                return new RamaRowerowa(53.1, 102, BMX, ALUMINIUM);
            case DOROSŁY:
            default:
                return new RamaRowerowa(60.0, 113, BMX, ALUMINIUM);
        }
    }

    public static RamaRowerowa stworzRameDoRoweruGorskiego(PrzeznaczenieWiekowe wiek) {
        switch (wiek) {
            case DZIECKO:
                return new RamaRowerowa(33.1, 103, GÓRSKI, ALUMINIUM);

            case NASTOLATEK:
                return new RamaRowerowa(51.6, 122, GÓRSKI, WLOKNO_WEGLOWE);

            default:
            case DOROSŁY:
                return new RamaRowerowa(61.1, 123, GÓRSKI, WLOKNO_WEGLOWE);
        }
    }
}
