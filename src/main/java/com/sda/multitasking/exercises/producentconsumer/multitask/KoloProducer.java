package com.sda.multitasking.exercises.producentconsumer.multitask;

import com.sda.multitasking.exercises.producentconsumer.Kolo;
import com.sda.multitasking.exercises.producentconsumer.KoloRowerowe;

public class KoloProducer extends Producer {

    public KoloProducer(Magazyn magazyn, long breakTime) {
        super(magazyn, breakTime);
    }

    @Override
    public void run() {
//        while (working) {
//            try {
//                 10 sekund przespania
//                 wątek wybudza się i tworzy obiekt Kolo
//                Thread.sleep(5000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            Kolo noweKolo = KoloRowerowe.stworzKoloBasic();
            magazyn.dodajKoloDoMagazynu(noweKolo);
            System.out.println("Stworzyłem koło i oddałem do magazynu.");
//        }
    }
}
