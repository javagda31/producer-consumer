package com.sda.multitasking.exercises.producentconsumer.multitask;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class Producer implements Runnable {

    protected boolean working;
    protected Magazyn magazyn;
    private long breakTime;

//    private Thread watek;
    private static ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

    public Producer(Magazyn magazyn, long breakTime) {
        this.magazyn = magazyn;
        this.breakTime = breakTime;
    }

    public void start() {
        working = true;
        executorService.scheduleAtFixedRate(this, 0, breakTime, TimeUnit.SECONDS);
//        watek = new Thread(this);
//        watek.start();
    }


    // trzeba zaimplementować w klasie końcowej
//    @Override
//    public void run() {
//
//    }
}
