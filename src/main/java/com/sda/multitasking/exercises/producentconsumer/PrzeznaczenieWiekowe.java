package com.sda.multitasking.exercises.producentconsumer;

public enum PrzeznaczenieWiekowe {
    DZIECKO, NASTOLATEK, DOROSŁY;
}
