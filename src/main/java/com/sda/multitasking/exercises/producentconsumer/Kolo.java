package com.sda.multitasking.exercises.producentconsumer;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Kolo {
    private double średnica;
    private double szerokość;
    private boolean opona;

}
