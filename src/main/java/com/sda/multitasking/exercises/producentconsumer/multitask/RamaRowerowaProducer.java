package com.sda.multitasking.exercises.producentconsumer.multitask;

import com.sda.multitasking.exercises.producentconsumer.PrzeznaczenieWiekowe;
import com.sda.multitasking.exercises.producentconsumer.RamaRowerowa;

public class RamaRowerowaProducer extends Producer {
    public RamaRowerowaProducer(Magazyn magazyn, long breakTime) {
        super(magazyn, breakTime);
    }

    @Override
    public void run() {
//        while (working) {
//            try {
//                 10 sekund przespania
        // wątek wybudza się i tworzy obiekt RamaRowerowa
//                Thread.sleep(8000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        RamaRowerowa rama = RamaRowerowa.stworzRameBMX(PrzeznaczenieWiekowe.NASTOLATEK);
        magazyn.dodajRameDoMagazynu(rama);
        System.out.println("Stworzyłem rame i oddałem do magazynu.");
//        }
    }
}
