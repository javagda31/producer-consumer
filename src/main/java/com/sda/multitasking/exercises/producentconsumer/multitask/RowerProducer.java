package com.sda.multitasking.exercises.producentconsumer.multitask;

import com.sda.multitasking.exercises.producentconsumer.Kolo;
import com.sda.multitasking.exercises.producentconsumer.RamaRowerowa;
import com.sda.multitasking.exercises.producentconsumer.Rower;

public class RowerProducer extends Producer {
    public RowerProducer(Magazyn magazyn, long breakTime) {
        super(magazyn, breakTime);
    }

    @Override
    public void run() {
//        while (working) {
//            try {
        if (magazyn.wystarczyKomponentówNaJedenRower()) {
            System.out.println("Producent ma wszystkie komponenty. Buduje rower.");
            Kolo[] kola = magazyn.pobierzKola();

            RamaRowerowa rama = magazyn.pobierzRame();

            Rower rower = new Rower(rama, kola, "Trek");

//                    Thread.sleep(5000);
            magazyn.dodajRowerDoMagazynu(rower);
        }
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
    }
}
