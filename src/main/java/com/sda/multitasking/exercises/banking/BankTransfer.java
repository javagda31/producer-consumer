package com.sda.multitasking.exercises.banking;

public class BankTransfer implements Runnable {
    private BankAccount konto;
    private double kwota;
    private KierunekPrzelewu kierunekPrzelewu;

    public BankTransfer(BankAccount konto, double kwota, KierunekPrzelewu kierunekPrzelewu) {
        this.konto = konto;
        this.kwota = kwota;
        this.kierunekPrzelewu = kierunekPrzelewu;
    }

    @Override
    public void run() {
        switch (kierunekPrzelewu) {
            case WYCHODZACY:
                konto.odejmij(kwota);
                break;
            case PRZYCHODZACY:
                konto.dodaj(kwota);
                break;
        }
    }
}
