package com.sda.multitasking.exercises.banking;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();

        for (int i = 0; i < 10000; i++) {
            if (i % 2 == 0) {
                bank.przeslijPieniadze(5, KierunekPrzelewu.WYCHODZACY);
            } else {
                bank.przeslijPieniadze(5, KierunekPrzelewu.PRZYCHODZACY);
            }
        }

        try {

            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        bank.wyswietlStan();
    }
}
