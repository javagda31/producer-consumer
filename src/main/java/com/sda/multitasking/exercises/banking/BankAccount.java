package com.sda.multitasking.exercises.banking;

public class BankAccount {
    private double stanKonta;
    private final Object lock = new Object(); // blokada

    public void dodaj(double kwota) {
        synchronized (lock) {
            stanKonta += kwota;
        }
    }

    public void odejmij(double kwota) {
        synchronized (lock) {
            stanKonta -= kwota;
        }
    }

    public synchronized void zmiana(double kwota, KierunekPrzelewu kierunekPrzelewu) {
//        synchronized (lock) {
        switch (kierunekPrzelewu) {
            case PRZYCHODZACY:
                stanKonta += kwota;
            case WYCHODZACY:
                stanKonta -= kwota;
        }
//        }
    }

    public void wyswietlBilans() {
        System.out.println(stanKonta);
    }
}
