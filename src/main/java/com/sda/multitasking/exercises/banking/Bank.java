package com.sda.multitasking.exercises.banking;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Bank {
    private ExecutorService pulaWątków = Executors.newFixedThreadPool(4);
    private BankAccount kontoBankowe = new BankAccount();

    public void przeslijPieniadze(double kwota, KierunekPrzelewu kierunekPrzelewu) {
        pulaWątków.submit(new BankTransfer(kontoBankowe, kwota, kierunekPrzelewu));
    }

    public void wyswietlStan() {
        kontoBankowe.wyswietlBilans();
    }
}
