package com.sda.multitasking.exercises.swing.okienko_robot;

public class Robot {
    private int poziomBaterii;
    private String nazwaRobota;
    private boolean wlaczony;
    private IEventLogger logger;

    public Robot(String nazwaRobota, IEventLogger logger) {
        this.poziomBaterii = 100;
        this.nazwaRobota = nazwaRobota;
        this.wlaczony = false;
        this.logger = logger;
    }

    public int getPoziomBaterii() {
        return poziomBaterii;
    }

    public boolean isWlaczony() {
        return wlaczony;
    }

    public void poruszRobotem(RuchRobota ruch) {
        if (!wlaczony) {
            logger.addMessage("Robot jest wyłączony, aby wykonać ruch, włącz robota.");
            return;
        }

        if (poziomBaterii >= ruch.getZuzycieBaterii()) {
            poziomBaterii -= ruch.getZuzycieBaterii();
            logger.addMessage("Wykonano ruch " + ruch + ", poziom baterii: " + poziomBaterii);
        } else {
            logger.addMessage("Niewystarczający poziom baterii. Wyłącz robota i go naładuj.");
        }
    }

    public void włącz() {
        if (wlaczony) {
            logger.addMessage("Robot jest już włączony.");
        } else {
            // onOff
            // boolean maybe_it_should_maybe_it_shouldnt = true;
            wlaczony = true;
            logger.addMessage("Uruchamiam robota.");
        }
    }

    public void wyłącz() {
        if (!wlaczony) {
            logger.addMessage("Robot jest już wyłączony.");
        } else {
            wlaczony = false;
            logger.addMessage("Wyłączam robota.");
        }
    }

    public void naładuj() {
        if (!wlaczony) {
            logger.addMessage("Ładuję robota.");
            poziomBaterii = 100;
        } else {
            logger.addMessage("Należy wyłączyć robota by go naładować.");
        }
    }
}
