package com.sda.multitasking.exercises.swing.okienko_robot;

import javax.swing.*;
import java.awt.*;

public class Okienko extends JFrame {
    private Robot robot;
    private JPanel panelGlowny;
    private PanelDolny panelDolny;
    private PanelGorny panelGorny;

    public Okienko() throws HeadlessException {
        this.panelGlowny = new JPanel(new GridBagLayout());
        setContentPane(panelGlowny);

        stworzIDodajPanelDolny();
        // przesuwam stworzenie robota na moment po stworzeniu panelu dolnego
        // (potrzebuję jego referencji jako Loggera)
        this.robot = new Robot("Marian", panelDolny);
        stworzIDodajPanelGorny(robot);

        setSize(640, 480);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void stworzIDodajPanelDolny() {
        panelDolny = new PanelDolny();

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints.gridy = 1; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 0.3;

        this.panelGlowny.add(panelDolny, constraints);
    }

    private void stworzIDodajPanelGorny(Robot robot) {
        panelGorny = new PanelGorny(robot);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints.gridy = 0; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 0.7;

        this.panelGlowny.add(panelGorny, constraints);
    }
}
