package com.sda.multitasking.exercises.swing.okienko_lista;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.awt.GridBagConstraints.*;

public class LeftPanel extends JPanel {
    private JLabel etykieta;
    private JTextField poleTekstowe;
    private JButton guzikAdd;
    private IListManager listManager;

    public LeftPanel(IListManager iListManager) {
        this.listManager = iListManager;
        setLayout(new GridBagLayout());

        dodajEtykiete();
        dodajPoleTekstowe();
        dodajPustyPanel();
    }

    private void dodajPustyPanel() {
        JPanel pustyPanel = new JPanel();
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.fill = BOTH;

        this.add(pustyPanel, gridBagConstraints);
    }

    private void dodajPoleTekstowe() {
        JPanel inputRow = new JPanel(); // stworzenie wiersza z polem tekstowym.
        inputRow.setLayout(new GridLayout(1, 2));

        poleTekstowe = new JTextField();
        inputRow.add(poleTekstowe); // pole tekstowe

        guzikAdd = new JButton("Dodaj do listy");
        guzikAdd.addActionListener(new AddButtonListener());
        inputRow.add(guzikAdd); // guzik

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        this.add(inputRow, gridBagConstraints); // dodanie wiersza z polem tekstowym
    }

    private void dodajEtykiete() {
        etykieta = new JLabel("Lewy panel:");
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.0;

        this.add(etykieta, gridBagConstraints);
    }

    private class AddButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            // todo : powrócimy po zaimplementowaniu panelu prawego.
            listManager.addElement(poleTekstowe.getText());
            poleTekstowe.setText("");
        }
    }
}
