package com.sda.multitasking.exercises.swing;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// Okno = okno
public class HelloWorldWindow extends JFrame {
    private JPanel panel1;
    private JButton button1;
    private JLabel etykieta;

    public HelloWorldWindow() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                etykieta.setText("Hello World !");
            }
        });

        this.setContentPane(panel1);
        this.setSize(300, 300);
    }
}
