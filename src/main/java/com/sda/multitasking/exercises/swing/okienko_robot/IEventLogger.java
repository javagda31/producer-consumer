package com.sda.multitasking.exercises.swing.okienko_robot;

public interface IEventLogger {
    void addMessage(String msg);
}
