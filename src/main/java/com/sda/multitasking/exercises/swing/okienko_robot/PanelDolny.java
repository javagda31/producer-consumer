package com.sda.multitasking.exercises.swing.okienko_robot;

import javax.swing.*;
import java.awt.*;

import static java.awt.GridBagConstraints.NORTHWEST;

public class PanelDolny extends JPanel implements IEventLogger{
    private JList<String> list;
    private DefaultListModel<String> listModel;
    private JScrollPane jScrollPane;

    public PanelDolny() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createLineBorder(Color.RED, 1));

        dodajEtykiete();
        dodajLogi();
    }

    private void dodajLogi() {
        listModel = new DefaultListModel<>();
        list = new JList<>(listModel);

        jScrollPane = new JScrollPane();
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane.setViewportView(list);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints.gridy = 1; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.anchor = GridBagConstraints.NORTHWEST;

        add(jScrollPane, constraints);
    }

    private void dodajEtykiete() {
        JLabel etykieta = new JLabel("Logi:");

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints.gridy = 0; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
//        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.weighty = 0.0;
        constraints.anchor = GridBagConstraints.NORTH;

        add(etykieta, constraints);
    }

    @Override
    public void addMessage(String msg) {
        listModel.addElement(msg);

        JScrollBar vertical = jScrollPane.getVerticalScrollBar();
        vertical.setValue( vertical.getMaximum() );
    }
}
