package com.sda.multitasking.exercises.swing.okienko_lista;

import javax.swing.*;
import java.awt.*;

public class Okno extends JFrame {
    private JPanel mainPanel;

    private LeftPanel leftPanel;
    private RightPanel rightPanel;

    public Okno() throws HeadlessException {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(1, 2));

        rightPanel = new RightPanel();
        leftPanel = new LeftPanel(rightPanel);

        mainPanel.add(leftPanel);
        mainPanel.add(rightPanel);

        // ustawienie panelu main jako treści okna.
        setContentPane(mainPanel);

        setSize(640, 480);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new Okno().setVisible(true);
    }
}
