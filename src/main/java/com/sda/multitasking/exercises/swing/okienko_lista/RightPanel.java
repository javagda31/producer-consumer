package com.sda.multitasking.exercises.swing.okienko_lista;

import javax.swing.*;
import java.awt.*;

import static java.awt.GridBagConstraints.BOTH;

public class RightPanel extends JPanel implements IListManager {
    private JLabel etykieta;
    private JList<String> list;
    private DefaultListModel<String> listModel;

    public RightPanel() {
        setLayout(new GridBagLayout());

        dodajEtykiete();
        dodajListe();
    }

    private void dodajListe() {
        list = new JList<>();
        listModel = new DefaultListModel<>();
        list.setModel(listModel);

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0; // pozycja na siatce
        gridBagConstraints.gridy = 1; // pozycja na siatce niżej niż etykieta
        gridBagConstraints.weightx = 1.0; // czy ma się rozszerzać i "kłócić" o miejsce w osi X
        gridBagConstraints.weighty = 1.0; // czy ma się rozszerzać i "kłócić" o miejsce w osi Y
        gridBagConstraints.fill = BOTH; // czy ma się rozszerzać i "kłócić" o miejsce w osi Y

        this.add(list, gridBagConstraints);
    }

    private void dodajEtykiete() {
        etykieta = new JLabel("Prawy panel:");
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0; // pozycja na siatce
        gridBagConstraints.gridy = 0; // pozycja na siatce
        gridBagConstraints.weightx = 1.0; // czy ma się rozszerzać i "kłócić" o miejsce w osi X
        gridBagConstraints.weighty = 0.0; // czy ma się rozszerzać i "kłócić" o miejsce w osi Y

        this.add(etykieta, gridBagConstraints);
    }

    @Override
    public void addElement(String element) {
        listModel.addElement(element);
    }
}
