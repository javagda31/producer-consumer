package com.sda.multitasking.exercises.swing.okienko_robot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PanelGorny extends JPanel {
    private JButton gWlacz;
    private JButton gWylacz;
    private JButton gNaladuj;

    private JComboBox<String> listaRozwijana;
    private JButton gPorusz;

    private JLabel etykietaPoziomBaterii;
    private JLabel etykietaStanWlaczenia;

    private Robot robot;

    public PanelGorny(Robot robot) {
        this.robot = robot;

        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));

        dodajGuzikWlacz();
        dodajGuzikWylacz();
        dodajGuzikNaladuj();
        dodajPanelKontroli();
        dodajPanelStanu();
    }

    private void dodajPanelStanu() {
        JPanel panel = new JPanel(new GridBagLayout());

        etykietaPoziomBaterii = new JLabel("Bateria: 100%");
        GridBagConstraints constraints1 = new GridBagConstraints();
        constraints1.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints1.gridy = 0; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints1.anchor = GridBagConstraints.WEST;
        constraints1.weightx = 1.0;
        constraints1.weighty = 0.0;
        panel.add(etykietaPoziomBaterii, constraints1);

        etykietaStanWlaczenia = new JLabel("Robot: Wyłączony");
        constraints1 = new GridBagConstraints();
        constraints1.gridx = 1; // pozycja w siatce, w osi X (prawo / lewo)
        constraints1.gridy = 0; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints1.anchor = GridBagConstraints.EAST;
        constraints1.weightx = 1.0;
        constraints1.weighty = 0.0;

        panel.add(etykietaStanWlaczenia, constraints1);

        // constraints - dodaję panel z etykietami do panelu gornego.
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints.gridy = 4; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.weighty = 0.0;
        constraints.insets = new Insets(5, 10, 5, 10);
        add(panel, constraints);
    }

    private void dodajPanelKontroli() {
        listaRozwijana = new JComboBox<>();
        for (RuchRobota value : RuchRobota.values()) {
            listaRozwijana.addItem(value.toString());
        }
        gPorusz = new JButton("Porusz robotem");
        gPorusz.addActionListener(actionEvent -> {
            Object selectedItem = listaRozwijana.getSelectedItem();
            String selectedOption = String.valueOf(selectedItem);

            RuchRobota ruch = RuchRobota.valueOf(selectedOption);
            robot.poruszRobotem(ruch);
            aktualizujStatus();
        });

        JPanel panel = new JPanel(new GridBagLayout());

        // constraints - dodaję listę rozwijaną do panelu
        GridBagConstraints constraints1 = new GridBagConstraints();
        constraints1.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints1.gridy = 0; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints1.anchor = GridBagConstraints.WEST;
        constraints1.weightx = 1.0;
        constraints1.weighty = 0.0;
        panel.add(listaRozwijana, constraints1);

        // constraints - dodaję guzik do panelu
        constraints1 = new GridBagConstraints();
        constraints1.gridx = 1; // pozycja w siatce, w osi X (prawo / lewo)
        constraints1.gridy = 0; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints1.anchor = GridBagConstraints.EAST;
        constraints1.weightx = 1.0;
        constraints1.weighty = 0.0;
        panel.add(gPorusz, constraints1);

        // constraints - dodaję panel z guzikiem i listą rozwijaną do panelu gornego.
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints.gridy = 3; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.weighty = 0.0;
        constraints.insets = new Insets(5, 10, 5, 10);
        add(panel, constraints);
    }

    private void dodajGuzikNaladuj() {
        gNaladuj = new JButton("Naładuj robota");
        gNaladuj.addActionListener(actionEvent -> {
            robot.naładuj();
            aktualizujStatus();
        });

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints.gridy = 2; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.weighty = 0.0;
        constraints.insets = new Insets(5, 10, 5, 10);

        add(gNaladuj, constraints);
    }

    private void dodajGuzikWlacz() {
        gWlacz = new JButton("Włącz robota");
        gWlacz.addActionListener(actionEvent -> {
            robot.włącz();
            aktualizujStatus();
        });

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints.gridy = 0; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.weighty = 0.0;
        constraints.insets = new Insets(5, 10, 5, 10);

        add(gWlacz, constraints);
    }

    private void dodajGuzikWylacz() {
        gWylacz = new JButton("Wyłącz robota");
        gWylacz.addActionListener(actionEvent -> {
            robot.wyłącz();
            aktualizujStatus();
        });

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0; // pozycja w siatce, w osi X (prawo / lewo)
        constraints.gridy = 1; // pozycja w siatce, w osi Y (gora / dol) [im więcej, tym niżej]
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.weighty = 0.0;
        constraints.insets = new Insets(5, 10, 5, 10);

        add(gWylacz, constraints);
    }

    private void aktualizujStatus() {
        etykietaStanWlaczenia.setText("Robot: " + (robot.isWlaczony() ? "Włączony" : "Wyłączony"));
        etykietaPoziomBaterii.setText("Bateria: " + robot.getPoziomBaterii() + "%");
    }
}
